#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

/*
Quien recibe conexiones (el servidor) enviará al cliente un
mensaje de bienvenida, después del cual, el cliente deberá
enviar un mensaje, y el servidor deberá recibir. Este
proceso terminará cuando en el servidor o en el cliente el
usuario ingrese un mensaje con el texto /exit".
*/
#define MSGSIZE 512

void usage(char * name);
int create_connection(char * ip, int port);
void handle_connection(int c);
int listen_server(int s,char * buf);
int speak_to_server(int s, char * buf);

int proccess_message(char * message);
int verificar( char *cadena, char *subcadena);

void usage(char * name){
	fprintf(stderr,
				"Usage: \n"
				"  %s IP PORT\n",name
				);
	exit(EXIT_FAILURE);

}
int main(int argc, char * argv[]) {
	int s;
	char * ip;
	int port;
	if(argc != 3){
		usage(argv[0]);
	}
	ip = argv[1];
	port = atoi(argv[2]);
	printf("ip: %s\n",ip );
	printf("port: %d\n",port );
	s = create_connection(ip,port);


	handle_connection(s);
	close(s);
	exit(EXIT_SUCCESS);
}
int create_connection(char * ip, int port){
	int s;
	struct sockaddr_in addr;
	if ( (s = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
		perror("socket");
		exit(EXIT_FAILURE);
	}
	memset(&addr, 0, sizeof(struct sockaddr_in));
	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	if (inet_aton(ip,&addr.sin_addr) == 0) {
		fprintf(stderr, "Invalid address!\n");
		exit(EXIT_FAILURE);
	}
	if (connect (s, (struct sockaddr *)&addr, sizeof(struct	sockaddr_in)) != 0) {
		perror("connect");
		exit(EXIT_FAILURE);
	}
	printf("paso de connect\n");
	return s;
}
void handle_connection(int s) {
	char buf[MSGSIZE];
	int finished;
	//Enviar al cliente un mensaje de bienvenida
	memset(buf, 0, MSGSIZE);
	read(s, buf, MSGSIZE);

	printf("Server sent: %s\n", buf);
	finished = 0;
	while (!finished) {
		memset(buf, 0, MSGSIZE);
		if((finished = speak_to_server(s,buf)) ==1){
			break;
		}
		memset(buf, 0, MSGSIZE);
		finished = listen_server(s,buf);
	}
	printf("adios..\n");
	close(s);
}

int listen_server(int s,char * buf){
	//leer mensaje del servidor
	if(read(s, buf, MSGSIZE)<MSGSIZE){
		printf("se perdieron datos leyendo...\n" );
	}
	//mostrar el mensaje
	printf("servidor: %s\n",buf);
	// si el mensaje del servidor comienza por "/exit",
	return proccess_message(buf);
}
int speak_to_server(int s, char * buf){
	int len_msg;
	//leer texto por la entrada estandar
	printf( "Enter a message :");
	fgets(buf,MSGSIZE,stdin);
	len_msg = strlen(buf);
	buf[len_msg-1] = '\0';
	//enviar texto al cliente
	if(write(s, buf, len_msg-1) <len_msg-1){
		printf("se perdieron datos escribiendo...\n" );
	}
	//si el texto leido comienza por "/exit", terminar.
	return proccess_message(buf);
}

int proccess_message(char * message){
	return verificar(message,"/exit");
}

int verificar( char *cadena, char *subcadena )
{
   char *tmp = cadena;
   char *pdest;

   pdest = strstr( tmp, subcadena );
   if( pdest ) return 1;

   return 0;
}
