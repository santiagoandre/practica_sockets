#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
/*
Quien recibe conexiones (el servidor) enviará al cliente un
mensaje de bienvenida, después del cual, el cliente deberá
enviar un mensaje, y el servidor deberá recibir. Este
proceso terminará cuando en el servidor o en el cliente el
usuario ingrese un mensaje con el texto /exit".
*/
#define MSGSIZE 512
void usage();
void start(int port);
int create_connection(int port);
void handle_connection(int pos);
int listen_client(int c,char * buf);
int speak_to_client(int c, char * buf);

int proccess_message(char * message);
int verificar( char *cadena, char *subcadena );
void usage(char * name){
	fprintf(stderr,
				"Usage: \n"
				"  %s PORT\n",name
				);
	exit(EXIT_FAILURE);

}
int main(int argc, char * argv[]) {
	int port;
	if(argc != 2){
		usage(argv[0]);
	}
	port = atoi(argv[1]);
	start(port);
}
void start(int port){
	int s;
	int c;

	s = create_connection(port);
	//ESPERA UNA CONECCION, ESPERA A UN CLIENTE
	printf("escuchando en el puerto %d\n", port);
	if (listen(s, 10) != 0) {
		perror("bind");
		exit(EXIT_FAILURE);
	}
	printf("paso de listen\n");
	//SE CONECTO UN CLIENTE
	c = accept(s, NULL, 0);//accept(s,addrc,0); addrc es la informacion del address del cliente que se conecto
	printf("paso de accept\n" );
	if(c == -1){
		perror("accept");
		exit(EXIT_FAILURE);
	}
	//ATENDER CONECCION
	//fflush(stdin);
	handle_connection(c);
	//CERRAR SOCKET
	close(s);
	//TERMINAR
	exit(EXIT_SUCCESS);
}
int create_connection(int port){
	struct sockaddr_in addr;
	int s;
	socklen_t len;

	//CREAR EL SOCKET S
	if ( (s = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
		perror("socket");
		exit(EXIT_FAILURE);
	}
	printf("se creo el socket\n" );

	//setsockopt(s, SOL_SOCKET, SO_REUSEADDR, (void *)&valopc,len);
	//CREAR ADDRESS, ESTA ES UN ADDRESS REMOTO, AQUI SE EPECIFICA LA DIRECCION Y EL PUERTO POR DONDE SE HACE LA COMUNICACION
	memset(&addr, 0, sizeof(struct sockaddr_in));
	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	addr.sin_addr.s_addr = INADDR_ANY;
	printf("se creo el address asociado al puerto %d\n",port);
	//SE ASOCIA EL SOCKET AL ADDRESS
	if (bind (s, (struct sockaddr *)&addr, sizeof(struct sockaddr_in)) != 0) {
		perror("bind");
		exit(EXIT_FAILURE);
	}
	printf("se asocio el addrr al socket\n");
	return s;
}
void handle_connection(int c) {
	char buf[MSGSIZE];
	int finished;
	//Enviar al cliente un mensaje de bienvenida
	strcpy(buf, "Hello!");
	write(c, buf, strlen(buf));
	finished = 1;
	while (finished) {
		memset(buf, 0, MSGSIZE);
		if(listen_client(c,buf)== 1){
			break;
		}
		memset(buf, 0, MSGSIZE);
		finished = !speak_to_client(c,buf);
	}
	printf("adios...\n" );
	close(c);
}
int listen_client(int c,char * buf){
	//leer mensaje del cliente
	if(read(c, buf, MSGSIZE)<MSGSIZE){
		printf("se perdieron datos leyendo...\n" );
	}
	//mostrar el mensaje
	printf("cliente: %s\n",buf);
	// si el mensaje del cliente comienza por "/exit",
	return  proccess_message(buf);
}
int speak_to_client(int c, char * buf){
	int len_msg;
	//leer texto por la entrada estandar
	printf( "Enter a message: ");
	fgets(buf,MSGSIZE,stdin);
	len_msg = strlen(buf);
	buf[len_msg-1] = '\0';
	//enviar texto al cliente
	if(write(c, buf, len_msg-1) <len_msg-1){
		printf("se perdieron datos escribiendo...\n" );
	}
	//si el texto leido comienza por "/exit", terminar.
	return proccess_message(buf);
}
int proccess_message(char * message){
	return verificar(message,"/exit");
}

int verificar( char *cadena, char *subcadena )
{
   char *tmp = cadena;
   char *pdest;

   pdest = strstr( tmp, subcadena );
   if( pdest ) return 1;

   return 0;
}
