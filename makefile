all: client.o server.o
	gcc -o client client.c
	gcc -o server server.c
client.o: client.c
	gcc -c -o client.o client.c
server.o: server.c
	gcc -c -o server.o server.c
clean:
	rm -f client server client.o server.o
